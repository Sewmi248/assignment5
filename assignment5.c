#include <stdio.h>

int noOfattend(int t_price){
    int base_price = 15;
    int attendees = 120;
    int price_difference = 5;
    int people_difference = 20;

    int d = t_price - base_price;
    int x = d / price_difference;

    if (t_price > base_price){
        attendees =attendees-(x * people_difference);
    }
    else if (t_price < base_price){
        attendees = attendees-(x * people_difference);}
    return attendees;
}

int revenue(int t_price){
    int a = noOfattend(t_price);
    return t_price * a;
}
int cost(int t_price){
    int b= noOfattend(t_price);
    int c = 500+(b*3);
    return c;
}

int profit(int t_price){
    int m =revenue(t_price);
    int n =cost(t_price);
    return m-n;
}

int main()
{
    int y =profit(20);

    printf("Profit : %d", y);

    return 0;
}
